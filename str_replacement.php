<?
/* Замена каждого вхождения строки по очереди на разные символы.*/
/* Например в коде ниже необходимо заменить в свойстве "content" значение на числовое, увеличивая его каждый раз на единицу.*/
$replacement = array( 
  			  'Главная' => 'Шины и диски', 
  			  'Каталог шин' => 'Шины', 
			  '">' => '/"><p itemprop="name" style="display:inline-block;">',
			  '//' => '/',
          'Каталог дисков' => 'Диски', 
          '<a' => '<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="breadcrumbs__item">
          <a itemprop="item" typeof="WebPage"', 
          '</a>' => '</p><meta itemprop="position" content="repi"></a></span></span>', 

          ); 	

 $g = 0;
$navLine = preg_replace_callback('~repi~s', function () use(&$g) { return  $g++ ; }, $replacement);
          echo $navLine;
?>